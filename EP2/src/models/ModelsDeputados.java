/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author joaoitalo
 */
public class ModelsDeputados {
    private String pesquisarDeputados;
    private String nome;
    private String estado;
    private String email;
    private String partido;
    private String telefone;
    private String sexo;
    private String nomeParlamentar;
    private String gabinete;
    private String anexo;
    private String linkFoto;
    private String UF;
    private String legislatura;
    private String NomePartido;
    private String IDPartido;

    

    /**
     * @return the pesquisarDeputados
     */
    public String getPesquisarDeputados() {
        return pesquisarDeputados;
    }

    /**
     * @param pesquisarDeputados the pesquisarDeputados to set
     */
    public void setPesquisarDeputados(String pesquisarDeputados) {
        this.pesquisarDeputados = pesquisarDeputados;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the partido
     */
    public String getPartido() {
        return partido;
    }

    /**
     * @param partido the partido to set
     */
    public void setPartido(String partido) {
        this.partido = partido;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the nomeParlamentar
     */
    public String getNomeParlamentar() {
        return nomeParlamentar;
    }

    /**
     * @param nomeParlamentar the nomeParlamentar to set
     */
    public void setNomeParlamentar(String nomeParlamentar) {
        this.nomeParlamentar = nomeParlamentar;
    }

    /**
     * @return the gabinete
     */
    public String getGabinete() {
        return gabinete;
    }

    /**
     * @param gabinete the gabinete to set
     */
    public void setGabinete(String gabinete) {
        this.gabinete = gabinete;
    }

    /**
     * @return the anexo
     */
    public String getAnexo() {
        return anexo;
    }

    /**
     * @param anexo the anexo to set
     */
    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    /**
     * @return the linkFoto
     */
    public String getLinkFoto() {
        return linkFoto;
    }

    /**
     * @param linkFoto the linkFoto to set
     */
    public void setLinkFoto(String linkFoto) {
        this.linkFoto = linkFoto;
    }

    /**
     * @return the UF
     */
    public String getUF() {
        return UF;
    }

    /**
     * @param UF the UF to set
     */
    public void setUF(String UF) {
        this.UF = UF;
    }

    /**
     * @return the legislatura
     */
    public String getLegislatura() {
        return legislatura;
    }

    /**
     * @param legislatura the legislatura to set
     */
    public void setLegislatura(String legislatura) {
        this.legislatura = legislatura;
    }

    /**
     * @return the NomePartido
     */
    public String getNomePartido() {
        return NomePartido;
    }

    /**
     * @param NomePartido the NomePartido to set
     */
    public void setNomePartido(String NomePartido) {
        this.NomePartido = NomePartido;
    }

    /**
     * @return the IDPartido
     */
    public String getIDPartido() {
        return IDPartido;
    }

    /**
     * @param IDPartido the IDPartido to set
     */
    public void setIDPartido(String IDPartido) {
        this.IDPartido = IDPartido;
    }
    
    
    


}
