/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author joaoitalo
 */
public final class ModeloTable extends AbstractTableModel{

    private static final long serialVersionUID = 1L;
     private ArrayList linhas = null;
    private String[] colunas = null;
    private String[] colunasDetalhes;
    
   
    public ModeloTable(ArrayList linhas, String[] colunas) {
        setLinhas(linhas);
        setColunas(colunas);
        
    }

    /**
     *
     * @param linhasDeDadosDetalhes
     * @param colunasDetalhes
     */
    
    /**
     *
     * @return
     */
    public ArrayList getLinhas() {
        return linhas;
    }

    
    public void setLinhas(ArrayList linhas) {
        this.linhas = linhas;
    }
    
    
    public String[] getColunas() {
        return colunas;
    }

    
    public void setColunas(String[] coluna) {
        this.colunas = coluna;
    }
    
    
    @Override
    public int getColumnCount(){
        return colunas.length;
    }

   
    @Override
    public int getRowCount(){
        return linhas.size();
    }
    
    
    @Override
    public String getColumnName(int numeroDaColuna) {
        return colunas[numeroDaColuna];
    }
    
    
    @Override
    public Object getValueAt(int numeroDaLinhas, int numeroDaColunas){
        Object[] linha = (Object[]) getLinhas().get(numeroDaLinhas);
        return linha[numeroDaColunas];
    }
}

